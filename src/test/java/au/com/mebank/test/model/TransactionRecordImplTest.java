package au.com.mebank.test.model;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.util.Date;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import au.com.mebank.test.factory.TransactionProcessorFactory;

public class TransactionRecordImplTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testTransactionRecordImpl() {
		String transactionId = "TX10001";
		String fromAccountId = "ACC334455"; 
		String toAccountId = "ACC778899";
		Date createAt = TransactionProcessorFactory.createDate("20/10/2018 12:47:55");
		BigDecimal amount = new BigDecimal("25.00");
		TransactionType transactionType = TransactionType.valueOf("PAYMENT");
		String relatedTransactionId = null;
		
		TransactionRecord record = new TransactionRecordImpl(transactionId, fromAccountId, toAccountId, createAt, amount, transactionType, relatedTransactionId);

		assertEquals(transactionId, record.getTransactionId());
		assertEquals(fromAccountId, record.getFromAccountId());
		assertEquals(toAccountId, record.getToAccountId());
		assertEquals(createAt, record.getCreateAt());
		assertEquals(amount, record.getAmount());
		assertEquals(transactionType, record.getTransactionType());
		assertEquals(relatedTransactionId, record.getRelatedTransactionId());
	}

}
