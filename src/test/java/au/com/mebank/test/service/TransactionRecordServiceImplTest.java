package au.com.mebank.test.service;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import au.com.mebank.test.factory.TransactionProcessorFactory;
import au.com.mebank.test.model.TransactionRecord;
import au.com.mebank.test.view.OutputStrategy;
import au.com.mebank.test.view.SystemOutputStrategy;

public class TransactionRecordServiceImplTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testReadCSV() {
		OutputStrategy outputStrategy = new SystemOutputStrategy();
		TransactionRecordService service = TransactionProcessorFactory.createTransactionRecordService(outputStrategy);
		
		String transactionText = 		
				"transactionId,fromAccountId,toAccountId,createdAt,amount,transactionType,relatedTransaction" + "\n"
				+ "TX10001,ACC334455,ACC778899,20/10/2018 12:47:55,25.00,PAYMENT";

		InputStream is = new ByteArrayInputStream(transactionText.getBytes());
		
		List<TransactionRecord> transactions = service.readCSV(is);
		
		assertEquals(1, transactions.size());
		assertEquals("TX10001", transactions.get(0).getTransactionId());
	}

	@Test
	public void testAnalyseTransactions() {
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		OutputStrategy outputStrategy = new SystemOutputStrategy(new PrintStream(outputStream));
		TransactionRecordService service = TransactionProcessorFactory.createTransactionRecordService(outputStrategy);
		
		String transactionText = 		
				"transactionId,fromAccountId,toAccountId,createdAt,amount,transactionType,relatedTransaction\n" + 
				"TX10001,ACC334455,ACC778899,20/10/2018 12:47:55,25.00,PAYMENT\n" + 
				"TX10002,ACC334455,ACC998877,20/10/2018 17:33:43,10.50,PAYMENT\n" + 
				"TX10003,ACC998877,ACC778899,20/10/2018 18:00:00,5.00,PAYMENT\n" + 
				"TX10004,ACC334455,ACC998877,20/10/2018 19:45:00,10.50,REVERSAL,TX10002\n" + 
				"TX10005,ACC334455,ACC778899,21/10/2018 09:30:00,7.25,PAYMENT";

		InputStream is = new ByteArrayInputStream(transactionText.getBytes());
		
		List<TransactionRecord> transactions = service.readCSV(is);
		
		service.analyseTransactions(
				transactions, 
				TransactionProcessorFactory.createDate("20/10/2018 12:00:00"), 
				TransactionProcessorFactory.createDate("20/10/2018 19:00:00"), 
				"ACC334455");
		
		String outputStreamAsString = new String(outputStream.toByteArray());
		assertEquals(
			"Number of transactions included is: 1\nRelative balance for the period is: -25.00\n", 
			outputStreamAsString);	
	}

}
