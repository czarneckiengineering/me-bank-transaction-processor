package au.com.mebank.test.factory;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import au.com.mebank.test.model.TransactionRecord;
import au.com.mebank.test.model.TransactionType;
import au.com.mebank.test.service.TransactionRecordService;
import au.com.mebank.test.view.OutputStrategy;
import au.com.mebank.test.view.SystemOutputStrategy;

public class TransactionProcessorFactoryTest extends TransactionProcessorFactory {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCreateTransactionRecordService() {		
		OutputStrategy outputStrategy = new SystemOutputStrategy();
		
		TransactionRecordService service = TransactionProcessorFactory.createTransactionRecordService(outputStrategy);
		
		assertNotNull(service);
	}

	@Test
	public void testCreateTransactionRecord() {
		String transactionId = "TX10001";
		String fromAccountId = "ACC334455"; 
		String toAccountId = "ACC778899";
		Date createAt = createDate("20/10/2018 12:47:55");
		BigDecimal amount = new BigDecimal("25.00");
		TransactionType transactionType = TransactionType.valueOf("PAYMENT");
		String relatedTransactionId = null;
		
		TransactionRecord record = TransactionProcessorFactory.createTransactionRecord(transactionId, fromAccountId, toAccountId, createAt, amount, transactionType, relatedTransactionId);

		assertEquals(transactionId, record.getTransactionId());
		assertEquals(fromAccountId, record.getFromAccountId());
		assertEquals(toAccountId, record.getToAccountId());
		assertEquals(createAt, record.getCreateAt());
		assertEquals(amount, record.getAmount());
		assertEquals(transactionType, record.getTransactionType());
		assertEquals(relatedTransactionId, record.getRelatedTransactionId());
	}

	@Test
	public void testCreateDate() {
		Date date = createDate("20/10/2018 12:47:55");
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		
		assertEquals(2018, calendar.get(Calendar.YEAR));
		assertEquals(Calendar.OCTOBER, calendar.get(Calendar.MONTH));
		assertEquals(20, calendar.get(Calendar.DAY_OF_MONTH));
		
		assertEquals(12, calendar.get(Calendar.HOUR_OF_DAY));
		assertEquals(47, calendar.get(Calendar.MINUTE));
		assertEquals(55, calendar.get(Calendar.SECOND));
		
	}

}
