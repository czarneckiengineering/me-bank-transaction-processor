package au.com.mebank.test;

import static org.junit.Assert.assertEquals;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import au.com.mebank.test.factory.TransactionProcessorFactory;

public class TransactionProcessorTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testMain() throws FileNotFoundException {
		String[] args = new String[] {"src/test/resources/test1.csv", "20/10/2018 12:00:00", "20/10/2018 19:00:00", "ACC334455"};
		
		TransactionProcessor.main(args);
	}

	@Test
	public void testAnalyseTransactions() throws FileNotFoundException {
		TransactionProcessor processor = new TransactionProcessor();
		
		processor.setAccountNumber("ACC334455");
		processor.setFromDate(TransactionProcessorFactory.createDate("20/10/2018 12:00:00"));
		processor.setToDate(TransactionProcessorFactory.createDate("20/10/2018 19:00:00"));
		processor.setIs(new FileInputStream("src/test/resources/test1.csv"));
		
		processor.analyseTransactions();
	}

	@Test
	public void testParseCommandLine() throws FileNotFoundException {
		TransactionProcessor processor = new TransactionProcessor();
		
		processor.parseCommandLine("src/test/resources/test1.csv", "20/10/2018 12:00:00", "20/10/2018 19:00:00", "ACC334455");
		
		assertEquals("ACC334455", processor.getAccountNumber());
		assertEquals(TransactionProcessorFactory.createDate("20/10/2018 12:00:00"), processor.getFromDate());
		assertEquals(TransactionProcessorFactory.createDate("20/10/2018 19:00:00"), processor.getToDate());
	}

}
