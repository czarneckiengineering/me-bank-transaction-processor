package au.com.mebank.test.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import au.com.mebank.test.factory.TransactionProcessorFactory;
import au.com.mebank.test.model.TransactionRecord;
import au.com.mebank.test.model.TransactionType;
import au.com.mebank.test.view.OutputStrategy;

public class TransactionRecordServiceImpl implements TransactionRecordService {
	
	private OutputStrategy outputStrategy;

	private static final String COMMA = ",";

	public TransactionRecordServiceImpl(OutputStrategy outputStrategy) {
		this.outputStrategy = outputStrategy;
	}

	@Override
	public List<TransactionRecord> readCSV(InputStream is) {
		List<TransactionRecord> transactions = new ArrayList<TransactionRecord>();
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(is));

			transactions = br.lines().skip(1).map(convertLineToTransactionRecord).collect(Collectors.toList());
			br.close();
			
		} catch (Throwable t) {
			System.out.println("IO failure while reading input match event data: " + t.getMessage());
		}
		return transactions;
	}
	
	
	
	private static Function<String, TransactionRecord> convertLineToTransactionRecord = (line) -> {
		String[] p = line.split(COMMA);// a CSV has comma separated lines

		String transactionId = p[0];
		
		String fromAccountId = p[1];
		String toAccountId = p[2];

		// I had a dilemma for date processing inside a lambda function - you can't throw an exception! 
		// A solution is at https://www.baeldung.com/java-lambda-exceptions but it looks pretty ugly to me so I will
		// assume files are relatively well formatted and just add null dates in the factory method
		
		Date createAt = TransactionProcessorFactory.createDate(p[3]);
		
		BigDecimal amount = new BigDecimal(p[4]);
		
		TransactionType transactionType = TransactionType.valueOf(p[5]);
		
		String relatedTransactionId = null;
		if (p.length > 6) {
			relatedTransactionId = p[6];
		}
		
		TransactionRecord record = TransactionProcessorFactory.createTransactionRecord(transactionId, fromAccountId, toAccountId, createAt, amount, transactionType, relatedTransactionId);
		return record;
	};

	@Override
	public BigDecimal analyseTransactions(List<TransactionRecord> transactions, Date fromDate, Date toDate, String accountId) {
		BigDecimal balance = BigDecimal.ZERO;
		
		try {
		
		// First get our list of reversals
		
		List<TransactionRecord> reversals = transactions.stream()
				.filter((t) -> 
					TransactionType.REVERSAL.equals(t.getTransactionType()))
				.collect(Collectors.toList());
		
		// build the set of related transaction ids - the original payments for each reversal
		
		Set<String> relatedTransactionIds = reversals.stream().map(TransactionRecord::getRelatedTransactionId).collect(Collectors.toSet());
		
		// build the filtered list of transactions - filter by accountId, date range, not in list of reversals, not a REVERSAL
		
		List<TransactionRecord> filteredTransactions = transactions.stream()
				.filter((t) -> 
					(accountId.equals(t.getFromAccountId()) || accountId.equals(t.getToAccountId())))
				.filter((t) ->
					fromDate.before(t.getCreateAt())
					&& toDate.after(t.getCreateAt()))
				.filter((t) ->
					!relatedTransactionIds.contains(t.getTransactionId()))
				.filter((t) -> 
					!TransactionType.REVERSAL.equals(t.getTransactionType()))
				.collect(Collectors.toList());
		
		// this is now the list of relevant PAYMENT transactions - so get the count
		
		long transactionCount = filteredTransactions.stream()
				.count();

		// add a starting element - set up the map-reduce in the case of zero transactions of a given type 
		
		filteredTransactions.add(0, TransactionProcessorFactory.createTransactionRecord(accountId, accountId, accountId, fromDate, BigDecimal.ZERO, TransactionType.PAYMENT, null));
		
		// grab the sum of deposits
		
		BigDecimal deposits = filteredTransactions.stream()
				.filter((t) -> 
					accountId.equals(t.getToAccountId()))
				.map((t) -> t.getAmount())
				.reduce((x,y) -> x.add(y)).get();
		
		// grab the sum of payments

		BigDecimal payments = filteredTransactions.stream()
				.filter((t) -> 
					accountId.equals(t.getFromAccountId()))
				.map((t) -> t.getAmount())
				.reduce((x,y) -> x.add(y)).get();
		
		// generate the delta on account balance
		
		balance = balance.add(deposits).subtract(payments);
		
		// print the required text summary - uses a strategy so that it is possible to test the output stream in automated tests 
		
		outputStrategy.transactionCount(transactionCount);
		outputStrategy.balance(balance);
		
		}
		catch (Throwable t) {
			System.out.println(t.getMessage());
		}

		return balance;
	}

}
