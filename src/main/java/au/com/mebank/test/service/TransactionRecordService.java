package au.com.mebank.test.service;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import au.com.mebank.test.model.TransactionRecord;

public interface TransactionRecordService {

	List<TransactionRecord> readCSV (InputStream is);

	BigDecimal analyseTransactions(List<TransactionRecord> transactions, Date fromDate, Date toDate, String accountNumber);
	
}
