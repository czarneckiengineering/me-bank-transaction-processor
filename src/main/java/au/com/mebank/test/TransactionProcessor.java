package au.com.mebank.test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Date;
import java.util.List;

import au.com.mebank.test.factory.TransactionProcessorFactory;
import au.com.mebank.test.model.TransactionRecord;
import au.com.mebank.test.service.TransactionRecordService;
import au.com.mebank.test.view.OutputStrategy;
import au.com.mebank.test.view.SystemOutputStrategy;

public class TransactionProcessor {
	
	private static OutputStrategy outputStrategy = new SystemOutputStrategy();
	
	private InputStream is;

	private Date fromDate;
	
	private Date toDate;
	
	private String accountNumber;

	private List<TransactionRecord> transactions;

	public static void main(String[] args) throws FileNotFoundException {
		if (args.length == 4) {
			TransactionProcessor transactionProcessor = new TransactionProcessor();
			
			transactionProcessor.parseCommandLine(args[0], args[1], args[2], args[3]);

			transactionProcessor.analyseTransactions();
		}
		else {
			outputStrategy.useage();
		}
	}

	public TransactionProcessor() {
	}

	public void analyseTransactions() {
		TransactionRecordService service = TransactionProcessorFactory.createTransactionRecordService(outputStrategy);
		
		transactions = service.readCSV(is);
		
		service.analyseTransactions(transactions, fromDate, toDate, accountNumber);
	}

	public void parseCommandLine(String csvFile, String fromDate, String toDate, String accountNumber) throws FileNotFoundException {
		is = new FileInputStream(csvFile);
		
		this.fromDate = TransactionProcessorFactory.createDate(fromDate);
		this.toDate = TransactionProcessorFactory.createDate(toDate);
		this.accountNumber = accountNumber;
	}

	static OutputStrategy getOutputStrategy() {
		return outputStrategy;
	}

	static void setOutputStrategy(OutputStrategy outputStrategy) {
		TransactionProcessor.outputStrategy = outputStrategy;
	}

	Date getFromDate() {
		return fromDate;
	}

	void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	Date getToDate() {
		return toDate;
	}

	void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	String getAccountNumber() {
		return accountNumber;
	}

	void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	List<TransactionRecord> getTransactions() {
		return transactions;
	}

	void setTransactions(List<TransactionRecord> transactions) {
		this.transactions = transactions;
	}

	InputStream getIs() {
		return is;
	}

	void setIs(InputStream is) {
		this.is = is;
	}

}
