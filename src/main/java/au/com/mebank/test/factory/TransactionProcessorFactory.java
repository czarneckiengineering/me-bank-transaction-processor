package au.com.mebank.test.factory;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import au.com.mebank.test.model.TransactionRecord;
import au.com.mebank.test.model.TransactionRecordImpl;
import au.com.mebank.test.model.TransactionType;
import au.com.mebank.test.service.TransactionRecordService;
import au.com.mebank.test.service.TransactionRecordServiceImpl;
import au.com.mebank.test.view.OutputStrategy;

public class TransactionProcessorFactory {
	
	public static TransactionRecordService createTransactionRecordService(OutputStrategy outputStrategy) {
		return new TransactionRecordServiceImpl(outputStrategy);
	}

	public static TransactionRecord createTransactionRecord(String transactionId, String fromAccountId, String toAccountId, Date createAt, BigDecimal amount, TransactionType transactionType, String relatedTransactionId) {
		return new TransactionRecordImpl( transactionId,  fromAccountId,  toAccountId,  createAt,  amount,  transactionType,  relatedTransactionId);
	}

	public static Date createDate(String dateString) {
		SimpleDateFormat parser = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

		try {
			return parser.parse(dateString);
		} catch (ParseException e) {
			return null;
		}
	}

}
