package au.com.mebank.test.model;

import java.math.BigDecimal;
import java.util.Date;

public interface TransactionRecord {

	String getRelatedTransactionId();

	TransactionType getTransactionType();

	BigDecimal getAmount();

	Date getCreateAt();

	String getToAccountId();

	String getFromAccountId();

	String getTransactionId();

}
