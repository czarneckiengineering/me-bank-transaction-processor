package au.com.mebank.test.model;

public enum TransactionType {
	PAYMENT,
	REVERSAL;
}
