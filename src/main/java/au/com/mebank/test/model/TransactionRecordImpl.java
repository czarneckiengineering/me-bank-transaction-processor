package au.com.mebank.test.model;

import java.math.BigDecimal;
import java.util.Date;

public class TransactionRecordImpl implements TransactionRecord {

	private String transactionId;
	private String fromAccountId;
	private String toAccountId; 
	private Date createAt;
	private BigDecimal amount;
	private TransactionType transactionType;
	private String relatedTransactionId;
	
	public TransactionRecordImpl(String transactionId, String fromAccountId, String toAccountId, Date createAt,
			BigDecimal amount, TransactionType transactionType, String relatedTransactionId) {
		this.transactionId = transactionId;
		this.fromAccountId = fromAccountId;
		this.toAccountId = toAccountId;
		this.createAt = createAt;
		this.amount = amount;
		this.transactionType = transactionType;
		this.relatedTransactionId = relatedTransactionId;
	}

	@Override
	public String getTransactionId() {
		return transactionId;
	}

	@Override
	public String getFromAccountId() {
		return fromAccountId;
	}

	@Override
	public String getToAccountId() {
		return toAccountId;
	}

	@Override
	public Date getCreateAt() {
		return createAt;
	}

	@Override
	public BigDecimal getAmount() {
		return amount;
	}

	@Override
	public TransactionType getTransactionType() {
		return transactionType;
	}

	@Override
	public String getRelatedTransactionId() {
		return relatedTransactionId;
	}

}
