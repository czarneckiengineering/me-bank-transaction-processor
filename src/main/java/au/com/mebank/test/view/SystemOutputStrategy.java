package au.com.mebank.test.view;

import java.io.PrintStream;
import java.math.BigDecimal;

public class SystemOutputStrategy implements OutputStrategy {

	private PrintStream printStream;

	public SystemOutputStrategy(PrintStream printStream) {
		super();
		this.printStream = printStream;
	}

	public SystemOutputStrategy() {
		this(System.out);
	}


	@Override
	public void useage() {
		printStream.println("Useage: java TransactionProcessor <CSV file> \"<from date>\" \"<to date>\" <account number>");
	}

	@Override
	public void balance(BigDecimal balance) {
		printStream.println("Relative balance for the period is: " + balance);
	}

	@Override
	public void transactionCount(long transactionCount) {
		printStream.println("Number of transactions included is: " + transactionCount);
	}

}
