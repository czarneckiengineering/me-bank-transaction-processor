package au.com.mebank.test.view;

import java.math.BigDecimal;

public interface OutputStrategy {
	
	void useage();
	
	void balance(BigDecimal balance);
	
	void transactionCount(long transactionCount);

}
